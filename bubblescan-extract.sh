#!/usr/bin/env bash

# Exit script if a program exit stats indicates an error
set -e
set -o pipefail

# Takes a filename as a parameter and echo's the parameter without a filename extension.
function remove_file_exten() {
	EXTEN=`echo "${1}" | awk -F . '{print $NF}'`
	echo "${1/%.${EXTEN}/}"
}

if [[ ! -e bubblescan.pdf ]]; then
	echo "Expecting file 'bubblescan.pdf' containing keys and students sheets"
	exit
fi

echo "Deleting any previously extracted images that may be present."
rm -f bubblescan-????.tif bubblescan-????-thresh.tif avg.tif


# If you get an error message here saying "not authorized", you may
# need to add read|write permissions to PDF files in
# /etc/ImageMagick/policy.xml
#
# <policy domain="coder" rights="read|write" pattern="PDF" />
echo "Exploding pdf"
convert -density 72 bubblescan.pdf -colorspace Gray -depth 8 -compress lzw bubblescan-%04d.tif
# Note: We intentionally explode PDF into low resolution images. It
# lets us focus on the larger dark blobs, makes file sizes tiny, and
# also speeds up processing later.

echo "Averaging (useful for finding grid bounds)"
convert -background transparent bubblescan-????.tif -average avg.tif

# Wait until there are less than N background jobs running. Not
# perfect, requires bash 4+, but generally works.
# https://stackoverflow.com/a/30396199
waitforjobs() {
    while test $(jobs -p | wc -w) -ge "$1"; do wait -n; done
}

echo "Converting each page into thresholded image"
for i in bubblescan-????.tif; do
	
	# 1. Normalize image so dark areas become black.
	#
	# 2. Apply a median filter to cut down noise. (Try adjusting
	# size). This step removes most of the blank bubbles.
	#
	# 3. Convolve a circle with the image. (Try adjusting size---it is
	# the radius of the circle in pixels.)  This step removes any
	# remaining noise.
	#
	# 4. Normalize the result.
	#
	# Note: Tiny, single pixel black spots or very light smudges over
	# a tiny area will be ignored. But, basically most smudges you see
	# in the output thresholded image will be considered as marks. See
	# countPixels() function in bubblescan.py for what it is configured
	# to ignore.
	base=`remove_file_exten "$i"`
	convert "$i" -contrast-stretch '1%x50%' -statistic Median 4x4 -morphology Correlate Disk:3.3 -normalize "$base-thresh.tif" &
	#echo "Now running jobs:"
	#jobs
	waitforjobs 4
	#echo "Ready for a new job:"
	#jobs
done

# wait for all jobs to finish
wait
