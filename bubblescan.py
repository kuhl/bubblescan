#!/usr/bin/env python3

# Author: Scott Kuhl
from wand.image import Image
from wand.color import Color
import numpy as np
import glob
import os


# === Coordinates ===
#
# Open avg.tif created by the extract script in an image editor which
# puts the 0,0 coordinate in the upper left corner of the image (such
# as GIMP). Then, imagine a grid being laid down that puts each bubble
# in its own cell. The first pair of numbers in these arrays is the
# CENTER of the upper left bubble on that grid. The second pair of
# numbers is the CENTER of the lower right corner of that grid. The
# last two numbers are the number of columns in the grid and the
# number of rows in the grid.
#
# If there is just one row or one column, the code assumes that the
# bubbles are roughly square shaped.
#
# A grid with 1 row and 1 column (simultaneously) is not supported.

#                upper left,  lower right, cols, rows
ansCoords =      [ 111,  74,   168, 696,    6,    50    ]
usernameCoords = [ 204, 156,   322, 599,    11,   26+10 ]
lastCoords =     [ 347, 157,   471, 477,    11,   26    ]
keyCoords =      [ 235, 106,   288, 106,    5,    1     ]


# === points ===
#
# How many points are each question worth? The length of this list
# only needs to match the length of the number of answers provided in
# the key.
points = [ 1 ] * 50  # make a list of 50 ones for an exam with 50 1pt questions with: [ 1 ] * 50

# 7 questions worth 4 points followed by 10 questions worth 3 points:
#points = [ 4 ] * 7 + [ 3 ] * 10

# manually list point values:
#points = [ 1 1 2 3 1 1 2 1 3 5 ]


# === ignorePixels ===
#
# If this is set to 1, then a single black pixel
# in the thresholded image will be ignored. Also, two 50% gray pixels
# will be ignored (because the total darkness would sum to match a
# single pixel). Increase this number to prevent the software from
# detecting an occasional stray smudge as an answer. Decrease the
# value to detect faint smudges as answers. It is recommended that you
# set this to as small of a value as you can without incorrectly
# identifying smudges as answers.
#
# Note: If there are multiple bubbles marked for a single question, we
# use the one that sums to the darkest value. Therefore, if there is a
# good mark and a faint smudge for an alternative answer, this
# software will detect the good mark. This variable will impact
# questions where there is no answer and we read a smudge as an
# answer.
ignorePixels = 2.5

# === Correcting mistakes after you have scanned copies ===
#
# You can correct minor mistakes (incorrectly scanned answers, missing
# keys, mistake on a key, etc) in the code near the bottom of this
# file file.



# Code follows. You should not need to make changes beyond this point.

class Exam:
    filename = None
    lastname = None
    username = None

    # A list of answers provided on the exam
    answers = None

    # Number of correct answers (ignoring point values)
    correctCount = 0
    # A list of the correct answers (to be compared with Exam.answers)
    correctAnswers = None

    totalPoints = 0

def pixel(image, x, y):
    return image[x][y];

def countWhite(image, minX,maxX,minY,maxY):
    Xr = range(minX, maxX+1)
    Yr = range(minY, maxY+1)
    count = 0;
    #print("counting %d %d %d %d" % (minX, maxX, minY, maxY))
    for x in Xr:
        for y in Yr:
            # negate pixel so filled in bubbles are bright.
            negatedPixel = 255-pixel(image,x,y)
            # Sum brightness of all pixels (gray pixels are added into sum).
            count = count+negatedPixel

    #print("returning %d" % count)
    #print(count)
    if count < 255*ignorePixels:
        return 0
    return count;

def layGrid(image, coord):
    """Given an image, the upper left corner, and lower left corner of an area on the image, the numbers of rows and columns in the area, return a 2D array showing how many white pixels are in the area."""

    upperLeftX = coord[0]
    upperLeftY = coord[1]
    lrX = coord[2]
    lrY = coord[3]
    cols = coord[4]
    rows = coord[5]
    

    # Calculate as float to prevent rounding errors in positioning of boxes

    
    # OLD: upper left is upper left corner of upper left bubble, lower
    # right is lower right corner of lower right bubble.
    # boxWidth = (lrX-upperLeftX)/float(cols)
    # boxHeight = (lrY-upperLeftY)/float(rows)

    # NEW: Upper left and lower right correspond to the CENTER of the bubbles.
    if cols > 1:
        boxWidth = (lrX-upperLeftX)/float(cols-1)
    if rows > 1:
        boxHeight = (lrY-upperLeftY)/float(rows-1)
    if cols < 1 or rows < 1:
        print("Invalid number of rows=%d or columns=%d" % (rows, cols))
        exit(1)
    if cols == 1 and rows == 1:
        print("row=1 and col=1 not currently supported.")
        exit(1)
    # If rows or cols is 1, assume bubbles are "square"
    if cols == 1:
        boxWidth = boxHeight
    if rows == 1:
        boxHeight = boxWidth
    

    
    grid = np.zeros((cols,rows), dtype='uint32')
    
    for r in range(rows):
        for c in range(cols):
            localULX=int(round(upperLeftX-0.5*boxWidth  + c*boxWidth))
            localULY=int(round(upperLeftY-0.5*boxHeight + r*boxHeight))
            grid[c][r] = countWhite(image, localULX, localULX+int(round(boxWidth)), localULY, localULY+int(round(boxHeight)))
    return grid


def largestColsInGrid(grid):
    """Given a grid (2D array), in each row find the column with the largest value. This is useful when one question has multiple choices arranged in a row."""
    largestCol=[ -1 for i in range(grid.shape[1]) ]
    for r in range(grid.shape[1]):
        maxVal = 0
        for c in range(grid.shape[0]):
            if grid[c][r] > maxVal:
                maxVal = grid[c][r]
                largestCol[r] = c
    return largestCol

def largestRowsInGrid(grid):
    """Given a grid (2D array), in each column find the row with the largest value. This is useful when one question has multiple choices arranged in a column."""
    largestRow=[ -1 for i in range(grid.shape[0]) ]
    for c in range(grid.shape[0]):
        maxVal = 0
        for r in range(grid.shape[1]):
            if grid[c][r] > maxVal:
                maxVal = grid[c][r]
                largestRow[c] = r
    return largestRow


def indexToLetterNumber(i):
    """Converts a number representing an answer to a space (no answer), an uppercase character, or a string with a number in it (for usernames that may include numbers)."""
    if i<0:            # no answer
        return " "
    if i >= 26 and i < 36:  # there are numbers after letters in username
        return str(i-26)
    else:   # number to letter in alphabet, uppercase
        return chr(i+65)

def letterToIndex(c):
    """Converts an answer (A, B, ..., F, etc) to a number (0, 1, ...)"""
    return ord(c.upper())-65

def readExam(filename):
    print("Processing %s" % filename)
    #tif = TIFF.open(filename, mode='r')
    #image = tif.read_image()
    
    image = Image(filename=filename)
    img_buffer=np.asarray(bytearray(image.make_blob(format='Gray')), dtype=np.uint8)
    img_buffer=np.reshape(img_buffer, (image.width, image.height), order='F')

    ex = Exam()
    ex.filename = filename

    # answers
    grid = layGrid(img_buffer, ansCoords)
    #print(grid)
    answers = largestColsInGrid(grid)
    #print(answers)
    ex.answers = answers

    # username
    grid = layGrid(img_buffer, usernameCoords)
    #print(grid)
    answers = largestRowsInGrid(grid)
    #print(answers)
    username = "" 
    for i in answers:
        username = username + indexToLetterNumber(i)
    ex.username = username.lower().strip()

    # lastname
    grid = layGrid(img_buffer, lastCoords)
    #print(grid)
    answers = largestRowsInGrid(grid)
    #print(answers)
    lastname = ""
    for i in answers:
        lastname = lastname + indexToLetterNumber(i)
    ex.lastname = lastname.lower().strip()

    # key
    grid = layGrid(img_buffer, keyCoords)
    answers = largestColsInGrid(grid)
    ex.key = answers[0]+1   # first key is key 1
    
    return ex
    
def readExams(filenames):
    exams = []
    for i in filenames:
        exams.append(readExam(i))
    return exams

def sortExams(exams):
    """Find keys in the exam (a key will have no name and no username)"""
    keys = []
    others = []

    keyNums = []
    
    for e in exams:
        if len(e.lastname.strip()) == 0 and len(e.username.strip()) == 0:
            keys.append(e)
            keyNums.append(e.key)
        else:
            others.append(e)

    print("Found %d keys (%s) and %d submissions out of %d pages" % (len(keys), str(keyNums), len(others), len(exams)))
    return (keys, others)


def getKey(keys, requestedKey):
    for e in keys:
        if e.key == requestedKey:
            return e
    print("Failed to find requested key: " + str(requestedKey))
    return None

def gradeExams(keys, students):
    for e in students:
        sol = getKey(keys, e.key)
        if sol == None:
            print("Missing key %d for exam by %s %s" % (e.key, e.username, e.lastname))
            return
        e.totalPoints = 0
        e.correctCount = 0
        e.correctAnswers = sol.answers

        for i in range(len(sol.answers)):  # for each answer
            if sol.answers[i] != -1:       # is there an answer to this question?

                # verify answer has a corresponding point value.
                if i >= len(points):
                    print("Key %d has an answer for question %d---but you have no point value assigned to it in points array." % (e.key, i+1))
                    exit(1)

                # Check student's answer
                if sol.answers[i] == e.answers[i]:
                    e.correctCount = e.correctCount+1
                    e.totalPoints  = e.totalPoints + points[i]

                    
def printExam(e, key=None):
    """Prints information about the exam to the console."""
    print("")
    print("%-11s %-8s - %3d points - %3d correct - key %d - %s" % (e.lastname, e.username, e.totalPoints, e.correctCount, e.key, e.filename))

    # We will calculate numQuestions so that it contains the number of
    # questions we should print out. If there are 50 spaces for
    # answers on the bubble sheet, but there are no student answers,
    # no correct answers, and no points assigned to the last 10
    # answers, the following code will calculate the number of
    # questions as 40.
    numQuestions = len(e.answers)
    while numQuestions > 0 and e.answers[numQuestions-1] < 0:
        numQuestions -= 1
    # keep blank answers if they have points assigned to them
    if numQuestions < len(points):
        numQuestions = len(points)
    # keep blank answers if there is an answer for them
    if e.correctAnswers:
        numAnswers = len(e.correctAnswers)
        while numAnswers > 0 and e.correctAnswers[numAnswers-1] < 0:
            numAnswers -= 1
        if numAnswers > numQuestions:
            numQuestions = numAnswers
    # Now, numQuestions should show the number of questions we should print out...

    
    # Write 2 digit question number above questions
    print("Q#  ", end="")
    for i in range(numQuestions):
        print("%-2d " % ((i+1)%100), end="")
    print("")

    # Write student answers, highlight wrong answers.
    print("ANS ", end="")
    for i in range(numQuestions):
        # If wrong
        if e.correctAnswers and e.correctAnswers[i] >= 0 and e.correctAnswers[i] != e.answers[i]:
            print("\033[91m\033[1m%s\033[0m  " % indexToLetterNumber(e.answers[i]).lower(), end='')
        else:   # If correct
            print("%s  " % indexToLetterNumber(e.answers[i]).lower(), end='')
            
    # Print correct answers below student answers (if needed)
    print("")
    if e.correctAnswers:
        print("KEY ", end="")
        for i in range(numQuestions):
            if e.correctAnswers[i] >= 0 and e.correctAnswers[i] != e.answers[i]:
                print("%s  " % indexToLetterNumber(e.correctAnswers[i]).lower(), end="")
            else:
                print("   ", end="")  # if student had correct answer, don't repeat it here.

                
def correctMistakeOnKey(keys, key, questionNumber, correctAnswer):
    # Apply corrections to key
    k = getKey(keys, key)
    oldAnswer = k.answers[questionNumber-1]   # question 1 is at index 0
    newAnswer = letterToIndex(correctAnswer)
    print("Key %d had question %d set to %s, changing it to %s" % (key, questionNumber, indexToLetterNumber(oldAnswer), correctAnswer));
    k.answers[questionNumber-1]=newAnswer
    
def getExamByName(studentExams, name):
    """Finds an exam in a list of exams which has the specified username, lastname, or filename."""
    for e in studentExams:
        if e.username == name or \
           e.lastname == name or \
           e.filename == name:
            return e
        

def verifyThresholdImagesUpdated():
    """Verifies that the threshold images are newer than the original bubblescan.pdf document. If the PDF is newer than the images, then the extraction script should probably be rerun."""
    if not os.path.exists("bubblescan.pdf") and not os.path.exists("bubblescan-0000-thresh.tif"):
        return

    # If the files needed to do the check are present...
    if os.stat("bubblescan.pdf").st_mtime > os.stat("bubblescan-0000-thresh.tif").st_mtime:
        print("bubblescan.pdf appears newer than the first thresholded image (bubblescan-0000-thresh.tif).")
        print("You may need to rerun the extraction script.")
        print()
        try:
            input("Press Enter to continue anyway or Ctrl+C to exit...")
        except KeyboardInterrupt:
            exit(1)
            pass
            
        
    

verifyThresholdImagesUpdated()        
inputFiles = glob.glob('bubblescan-????-thresh.tif')
inputFiles.sort()
exams = readExams(inputFiles)

#for e in exams:
#    printExam(e)

(keys, students) = sortExams(exams)

# HOW TO CORRECT A KEY WITH MISTAKES:
# Use the following to correct any mistakes that you might have on
# your answer key(s). In the commented out lines of code below, we
# change the answers to question 4 on answer keys 1, 2, and 3.
#correctMistakeOnKey(keys, 1, 4, 'd')
#correctMistakeOnKey(keys, 2, 4, 'b')
#correctMistakeOnKey(keys, 3, 4, 'a')

# HOW TO CORRECT AN EXAM WITH NO KEY OR MIS-SCANNED ANSWERS:
#toFix = getExamByName(students, "someusername")   # works with username, lastname, filename

# Set a missing key or wrong key.
#toFix.key = 2

# Change the answer to question 1. (Note array is 0 indexed. So
# subtract 1 from the question you wish to correct before indexing
# into the answers array). We use numbers to represent which answer
# the user selected. The letterToIndex converts from letters back into
# the numbers we store in the answers array.
# toFix.answers[0]=letterToIndex('A')


print("")
print("====== Keys ======")
# Sort keys before printing
keys.sort(key=lambda e: e.key, reverse=False)  # sort by key number
for e in keys:
    printExam(e)

    
gradeExams(keys, students)


print("")   
print("====== Submissions ======")
students.sort(key=lambda e: e.lastname, reverse=False)  # sort by last name
#students.sort(key=lambda e: e.key, reverse=False)  # sort by key
#students.sort(key=lambda e: e.username, reverse=False)  # sort by key
#students.sort(key=lambda e: e.totalPoints, reverse=False)  # sort by key
for e in students:
    printExam(e)



# ---- PRINT SUMMARY ----
print("")
print("====== Summary ======")

def percent(points, total):
    pcnt = 0
    denom = float(total)
    if denom > 0:
        pcnt = points/denom*100
    return pcnt


print("%-11s %-11s %3s %5s %3s %3s %s" % ("lastname", "username", "pts", "pcnt", "#c", "key", "filename"))
for e in students:
    print("%-11s %-11s %3d %5.1f %3d %1d %s" % (e.lastname, e.username, e.totalPoints, percent(e.totalPoints, sum(points)), e.correctCount, e.key, e.filename))

# ---- WRITE FILE ----
    
with open("grades.csv", 'w') as f:
    f.write("%s,%s,%s,%s,%s,%s,%s\n" % ("username", "lastname", "points", "percent", "num correct questions", "key", "filename"))
    for e in students:
        f.write("%s,%s,%d,%f,%d,%d,%s\n" % (e.username, e.lastname, e.totalPoints, percent(e.totalPoints, sum(points)), e.correctCount, e.key, e.filename))

print("Total points possible: %d" % sum(points))        
print("Wrote grades.csv")
